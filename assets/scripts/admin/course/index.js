"use strict";

$(document).ready(function () {
    
    dataList.DataTable({
        language: {url: "assets/bower_components/datatables.net/Thai.json"},
        serverSide: true,
        bFilter: false,
        ajax: {
            url: "admin/"+controller+"/data_index",
            type: 'POST',
            data: {csrfToken:get_cookie('csrfCookie'), frmFilter:(function(){return $("#frm-filter").serialize()})},
        },
        order: [[6, "DESC"]],
        pageLength: 10,
        columns: [
           {data: "dragDrop", width: "10px", className: "text-center", orderable: false},
            {data: "checkbox", width: "20px", className: "text-center", orderable: false},
            {data: "title",width: "", className: "", orderable: true},
             {data: "manage",width: "370px", className: "", orderable: false},
            {data: "active", width: "50px", className: "text-center", orderable: false},
            {data: "action", width: "30px", className: "text-center", orderable: false},
            {data: "order", width: "30px", className: "text-center handle", orderable: false, visible:false},
        ]
    }).on('draw', function () {
        $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt)
        $('.tb-check-single').iCheck(iCheckboxOpt)
    }).on('processing', function(e, settings, processing) {
        if ( processing ) {
            $('#overlay-box').removeClass('hidden');
        } else {
            $('#overlay-box').addClass('hidden');
        }
    })

    // Drag and drop
    var tbody = $(dataList).find('tbody')
    var sortOrder = [];
    $(tbody).sortable({
        placeholder: 'ui-state-highlight',
        forcePlaceholderSize: true,
        opacity: 0.6,
        axis: "y",
        items: 'tr',
        handle: '.handle',
        cursor: 'move',
        helper: fixHelperModified,
        start: function(e, ui){
            ui.placeholder.height(ui.item.height());
            $.map($('tr [name^=order]', tbody), function(element){
                sortOrder.push(element.value)
            })

            //console.log(sortOrder);
        },
        stop: function(e, ui) {
            $('#overlay-box').removeClass('hidden');
            $.each($('tr [name^=order]', tbody), function(index, element){
                element.value = sortOrder[index]
            })
            sortOrder = []
            $.each($('tr', tbody), function(index, element){
                sortOrder.push({courseId:$(element).attr('id'),order:$(element).find('[name^=order]').val()})
            })            
            $.post("admin/"+controller+"/update_order",{sortOrder:sortOrder,csrfToken:get_cookie('csrfCookie')},function(){
                $('#overlay-box').addClass('hidden');
            })
            sortOrder = []
        }
    })
    $(tbody).disableSelection()
    
})

function fixHelperModified(e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
}

 // {data: "createDate", width: "100px", className: "", orderable: true},
            // {data: "updateDate", width: "100px", className: "", orderable: true},

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})
