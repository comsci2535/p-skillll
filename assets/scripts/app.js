"use strict";
$(document).ready(function () {
    $(".select2").select2({
        width: "100%", language: "th"
    });
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            setTimeout(function() {
                $(this).toggleClass('open');        
            }, 1000)
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            setTimeout(function() {
                $(this).toggleClass('open');        
            }, 1000)     
        }
    ); 
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
    $(window).scrollTop() >= 65 ? $(".main-header").addClass("header--scrolled") : $(".main-header").removeClass("header--scrolled")
})
