 
$(document).ready(function() { 
    $(document).on('click','.btn-click-categories', function(){
        var $this = $(this);
        $this.closest('.primary-tags').find('a').removeClass('active'); 
        $this.addClass('active'); 
        var categoryId = $this.attr('data-ss-tag-slug');
        $.post('ajax_course_data',
        {
            categoryId   : categoryId
            ,csrfToken   :get_cookie('csrfCookie')
        }).done(function (result) {
            $('#display-course-all').html(result);
        }).fail(function(){
        
        })
    })

    $(document).on('click','.btn-click-news', function(){
        var $this = $(this);
        $this.closest('.primary-tags').find('a').removeClass('active'); 
        $this.addClass('active'); 
        var categoryId = $this.attr('data-ss-tag-slug');
        $.post('ajax_news_data',
        {
            categoryId   : categoryId
            ,csrfToken   :get_cookie('csrfCookie')
        }).done(function (result) { 
            $('#display-news-all').html(result);
        }).fail(function(){
          
        })
      })

    
});
