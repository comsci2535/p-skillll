 
<div class="side-sticky-menu-wrapper">
	<div class="side-sticky-menu">
		<div class="content">
			<div class="content-body">
				<div class="tag-link-wrapper browse-offerings-nav reset-tag clear <?php echo empty($categoryID) ? 'selected': '';?>">
					<a href="<?php echo base_url('course');?>" class="browse-offerings-link left">All Courses</a>
				</div>
				<div class="section-name">CREATE</div>
				<?php   
				if(!empty($category)):
					foreach($category as $key => $item):

						if($categoryID == $item->categoryId):
							$selected = 'selected';
						else:
							$selected = '';
						endif;
				?>
				<div class="tag-link-wrapper primary-tag-link-wrapper relative clear <?=$selected?>">
					<a href="<?php echo !empty($item->slug) ? 'course/'.$item->slug : '';?>" class="tag-link primary-tag related-tag left" data-ss-tag-slug="<?php echo !empty($item->categoryId) ? $item->categoryId : '';?>"><?php echo !empty($item->name) ? $item->name : '';?></a>
				</div>
				<?php
					endforeach;
				endif;
				?>
			</div>
		</div>
	</div>
</div> 