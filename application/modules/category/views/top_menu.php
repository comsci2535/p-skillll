
<nav id="nav-menu-container">
	<ul class="nav-menu">
		<li class="menu-has-children"><a href="javascript:void(0)">Categories</a>
			<ul >
				<?php
				if(!empty($category)):
					foreach($category as $key => $item):
				?>
				<li>
					<a href="<?php echo !empty($item->slug) ? 'course/'.$item->slug : '';?>"><?php echo !empty($item->name) ? $item->name : '';?></a>
				</li>  
				<?php
					endforeach;
				endif;
				?>              
			</ul> 
		</li> 
	</ul>
</nav>