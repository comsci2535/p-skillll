
<div class="row d-flex justify-content-center">
	<div class="menu-content pb-70 col-lg-12 text-center">
		<div class="title ">
		<h1 class="mb-10">Popular Courses we offer</h1>
			<p>There is a moment in the life of any aspiring.</p>
		</div>
		<div class="primary-tags">
			<a href="#all" class="primary-tag all-categories active btn-click-categories" data-ss-tag-slug="all">All Categories</a>
			<?php
			if(!empty($category)):
                    foreach($category as $item):
				?>
			<a href="#<?php echo !empty($item->slug) ? $item->slug : '';?>" class="primary-tag design btn-click-categories" data-ss-tag-slug="<?php echo !empty($item->categoryId) ? $item->categoryId : '';?>"> 
			<?php echo !empty($item->name) ? $item->name : '';?></a>
			<?php
				endforeach;
			endif;
			?>
		</div>
	</div>
</div>     