<?php echo Modules::run('banner/hero','home') ?>  
<!-- Start popular-course Area -->
<section class="popular-course-area section-gap">
  <div class="container">
    <?php echo Modules::run('category/home') ?>
    <div id="display-course-all" class="row">
      <?php echo Modules::run('course/home') ?>
    </div>
    <div class="row d-flex justify-content-center">
      <div class="menu-content text-center">  
          <a href="<?php echo base_url('course')?>" class="details-btn d-flex justify-content-center align-items-center details-btn-all">
              <span>All Courses</span>
              <span class="lnr lnr-arrow-right"></span>
          </a>  
      </div>
    </div>  
  </div>  
</section>
<!-- End popular-course Area --> 

<!-- Start blog Area -->
<section class="blog-area section-gap" id="blog" style="background-color: #f9f9ff">
  <div class="container">
    <?php echo Modules::run('news/home_news') ?>
    <div id="display-news-all" class="row">
      <?php echo Modules::run('news/home_list') ?>
    </div>
    <div class="row d-flex justify-content-center">
      <div class="menu-content text-center">  
          <a href="<?php echo base_url('news')?>" class="details-btn d-flex justify-content-center align-items-center details-btn-all">
              <span>All News</span>
              <span class="lnr lnr-arrow-right"></span>
          </a>  
      </div>
    </div>  
  </div>  
</section>