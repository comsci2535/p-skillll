  <!DOCTYPE html>
  <html lang="zxx" class="no-js">
  <head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <base href="<?php echo site_url(); ?>">
    <title><?php echo $pageTitle; ?></title>
    <meta name="description" content="<?php echo $metaDescription; ?>" />
    <meta name="keywords" content="<?php echo $metaKeyword; ?>" />
    <meta name="author" content="" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url("assets/website/") ?>images/favicon.ico" type="image/x-icon">

    <?php echo Modules::run('social/share'); ?>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
      <!--
      CSS
      ============================================= -->
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/linearicons.css">
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/font-awesome.min.css">
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/magnific-popup.css">
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/nice-select.css">              
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/animate.min.css">
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/owl.carousel.css">     
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/jquery-ui.css">      
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/main.css">
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/custom.css">
      <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/responsive.css">


     <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>unitegallery-master/package/unitegallery/css/unite-gallery.css?v=2.1.5' type='text/css' />

    <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.min.css?v=2.1.5' type='text/css' />


    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.css?v=2.1.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    <link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />

 
    <!-- <link rel="stylesheet" href="https://cdn.plyr.io/3.4.4/plyr.css"> -->
    <link rel='stylesheet' href='<?php echo base_url("assets/website/") ?>plyr-master/dist/plyr.css' type='text/css' />
    </head>
    <body>  
      
     
        <?php $this->load->view('topmenu'); ?>

      
      <!-- End blog Area -->      
       <?php $this->load->view($contentView); ?>

        <?php $this->load->view('footer'); ?>


      <script src="<?php echo base_url("assets/website/") ?>js/vendor/jquery-2.2.4.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="<?php echo base_url("assets/website/") ?>js/vendor/bootstrap.min.js"></script>      
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="<?php echo base_url("assets/website/") ?>js/easing.min.js"></script>      
      <script src="<?php echo base_url("assets/website/") ?>js/hoverIntent.js"></script>
      <script src="<?php echo base_url("assets/website/") ?>js/superfish.min.js"></script> 
      <script src="<?php echo base_url("assets/website/") ?>js/jquery.ajaxchimp.min.js"></script>
      <script src="<?php echo base_url("assets/website/") ?>js/jquery.magnific-popup.min.js"></script> 
        <script src="<?php echo base_url("assets/website/") ?>js/jquery.tabs.min.js"></script>           
      <script src="<?php echo base_url("assets/website/") ?>js/jquery.nice-select.min.js"></script>  
      <script src="<?php echo base_url("assets/website/") ?>js/owl.carousel.min.js"></script>                  
      <script src="<?php echo base_url("assets/website/") ?>js/mail-script.js"></script> 
      <script src="<?php echo base_url("assets/website/") ?>js/main.js"></script>  

       <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/jquery.fancybox.js?v=2.1.5" ></script>
      <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
      <script src="<?php echo base_url("assets/website/") ?>fancyapps-fancyBox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

       <?php if (in_array($this->router->class, array('course'))) : ?>
        <script src="<?php echo base_url("assets/website/") ?>vimeo-player/dist/player.js"></script>
        <script src="<?php echo base_url("assets/website/") ?>plyr-master/dist/plyr.js"></script>
        <script src="https://player.vimeo.com/api/player.js"></script>
      <?php endif; ?>

      <script type='text/javascript' src='<?php echo base_url("assets/website/") ?>sweetalert2/package/dist/sweetalert2.all.min.js'></script>

      <?php //if (in_array($this->router->class, array('user'))) : ?>
<script>
  var recaptcha_sitekey = "<?php echo $recaptcha_sitekey ?>";
  var recaptcha_secretkey = "<?php echo $recaptcha_secretkey ?>";
</script>
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>
<?php //endif; ?>


<script>
   

    function get_cookie(name) {

        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0)
                return c.substring(nameEQ.length, c.length);
        }
        return null;
    }
   

    
    var csrfToken = get_cookie('csrfCookie');
    var siteUrl = "<?php echo site_url(); ?>";
    var baseUrl = "<?php echo base_url(); ?>";
    var controller = "<?php echo $this->router->class ?>";
    var method = "<?php echo $this->router->method ?>";


    $(document).ready(function () {
        <?php if ($this->session->toastr) : ?>
            setTimeout(function () {
                toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
            }, 500);
            <?php $this->session->unset_userdata('toastr'); ?>
        <?php endif; ?>
    }); 



    function setLoginAfterurl(url){
        $("#after_login_url").val(url);
    }

    



      // Vanilla JavaScript
      var jsSocialShares = document.querySelectorAll(".js-social-share");
      if (jsSocialShares) {
        [].forEach.call(jsSocialShares, function(anchor) {
          anchor.addEventListener("click", function(e) {
            e.preventDefault();

            windowPopup(this.href, 500, 300);
          });
        });
      }



</script>

<?php echo $pageScript; ?>

<script src="<?php echo base_url(); ?>assets/scripts/front/login.js"></script>

    </body>
  </html>