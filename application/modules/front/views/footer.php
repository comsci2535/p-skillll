    <input type="hidden" name="after_login_url" id="after_login_url" value="<?php echo site_url('');?>">
            
      <!-- start footer Area -->    
      <footer class="footer-area section-gap">
        <div class="container">
          <div class="row">
            <div class="col-lg-2 col-md-6 col-sm-6">
              <div class="single-footer-widget">
                <h4>Top Products</h4>
                <ul>
                  <li><a href="#">Managed Website</a></li>
                  <li><a href="#">Manage Reputation</a></li>
                  <li><a href="#">Power Tools</a></li>
                  <li><a href="#">Marketing Service</a></li>
                </ul>               
              </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6">
              <div class="single-footer-widget">
                <h4>Quick links</h4>
                <ul>
                  <li><a href="#">Jobs</a></li>
                  <li><a href="#">Brand Assets</a></li>
                  <li><a href="#">Investor Relations</a></li>
                  <li><a href="#">Terms of Service</a></li>
                </ul>               
              </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6">
              <div class="single-footer-widget">
                <h4>Features</h4>
                <ul>
                  <li><a href="#">Jobs</a></li>
                  <li><a href="#">Brand Assets</a></li>
                  <li><a href="#">Investor Relations</a></li>
                  <li><a href="#">Terms of Service</a></li>
                </ul>               
              </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6">
              <div class="single-footer-widget">
                <h4>Resources</h4>
                <ul>
                  <li><a href="#">Guides</a></li>
                  <li><a href="#">Research</a></li>
                  <li><a href="#">Experts</a></li>
                  <li><a href="#">Agencies</a></li>
                </ul>               
              </div>
            </div>                                    
            <div class="col-lg-4  col-md-6 col-sm-6">
              <div class="single-footer-widget">
                <h4>Newsletter</h4>
                <p>Stay update with our latest</p>
                <div class="" id="mc_embed_signup">
                   <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get">
                    <div class="input-group">
                      <input type="text" class="form-control" name="EMAIL" placeholder="Enter Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email Address '" required="" type="email">
                      <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">
                          <span class="lnr lnr-arrow-right"></span>
                        </button>    
                      </div>
                        <div class="info"></div>  
                    </div>
                  </form> 
                </div>
              </div>
            </div>                      
          </div>
          <div class="footer-bottom row align-items-center justify-content-between">
            <p class="footer-text m-0 col-lg-6 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-6 col-sm-12 footer-social">
              <a href="#"><i class="fa fa-facebook"></i></a>
              <a href="#"><i class="fa fa-twitter"></i></a>
              <a href="#"><i class="fa fa-dribbble"></i></a>
              <a href="#"><i class="fa fa-behance"></i></a>
            </div>
          </div>            
        </div>
      </footer> 
      <!-- End footer Area -->  

      
<!-- Modal -->
      <div class="modal fade two-panel-signup-popup center-vertically" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            
            <div class="modal-body">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <div class="two-panel-signup-wrapper row">
              <!--   <div class="details-column-wrapper col-md-4">
                  <div class="details-column">
                    <div class="details-text">
                      <h3>Welcome Back to Skillll</h3>
                      <div class="header-separator"></div>
                      <h4>Sign in to continue to your account.</h4>
                      <p class="panel-tos"></p>
                    </div>
                  </div>
                </div> -->
                <div class="signup-login-column signup-form col-md-12">
                  
                    <div class="login-wrapper">
                      <div class="login-form-wrapper form-wrapper">
                        <a href="<?=$authUrl?>" class="btn large fb js-fb-login full-width">
                          <label class="facebook-logo"></label>
                          <span class="facebook-signup-button-text">Sign In with Facebook</span>
                        </a>
                        <div class="g-signin">
                          <a href="javascript:;" class="btn large google js-google-login full-width">
                            <label class="google-logo"></label>
                            <span class="googleSignInButtonText">Sign In with Google</span>
                          </a>
                        </div>
                        <div class="form-separator">
                          <p>or</p>
                        </div>
                        <?php
                          $form=array('accept-charset'=>'utf-8','class'=>'','id'=>'frm-login','name'=>'frm-login'); 
                          echo form_open('/user/login/check', $form);
                          echo form_hidden('base_url',base_url());
                          echo form_hidden('site_url',site_url());
                        ?>
                        <div class="alert error overview login-general-error"></div>
                        <fieldset>
                          <div class="fields input-wrapper">
                            <input placeholder="Email address" aria-label="Email address" type="text" name="username" id="username_login" class="login-form-email-input" tabindex="1">
                            <p class="error-message"></p>
                          </div>
                        </fieldset>
                        <fieldset>
                          <div class="fields input-wrapper">
                            <input placeholder="Password" aria-label="Password" type="password" name="password" id="password_login" class="login-form-password-input" tabindex="2">
                            <p class="error-message"></p>
                          </div>
                        </fieldset>
                        <div id="form-success-div_login" class="text-success"></div> 
                       <div id="form-error-div_login" class="text-danger"></div>
                        <div class="forgot-password">
                          <a class="secondary small" target="_blank" href="/reset-password">Forgot password?</a>
                        </div>
                        <input type="submit" name="login" class="login btn-login button large full-width btn-login-submit" tabindex="4" value="Sign In">
                        <div class="fields remember-me">
                          <input type="checkbox" name="LoginForm[rememberMe]" class="login-form-remember-me" id="login-form-remember-me" tabindex="5">
                          <label for="login-form-remember-me">Keep me signed in until I sign out</label>
                        </div>
                       </div>
                       <?php echo form_close(); ?>
                        <div class="popup-note text-center">
                          Not a member? <a class="js-switch-to-signup" href="<?php echo site_url("user/register/") ?>">Sign up.</a>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
            
          </div>
        </div>
      </div>