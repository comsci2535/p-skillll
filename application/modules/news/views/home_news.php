<div class="row d-flex justify-content-center">
	<div class="menu-content pb-70 col-lg-12 text-center">
		<div class="title ">
			<h1 class="mb-10">Latest posts from our Blog</h1>
			<p>In the history of modern astronomy there is.</p>
		</div>
		<div class="primary-tags">
			<a href="#all" class="primary-tag all-categories active btn-click-news" data-ss-tag-slug="all">All News</a>
			<?php
			if(!empty($news)):
                foreach($news as $item):
			?>
			<a href="#<?php echo !empty($item->slug) ? $item->slug : '';?>" class="primary-tag design btn-click-news" data-ss-tag-slug="<?php echo !empty($item->categoryId) ? $item->categoryId : '';?>"> 
			<?php echo !empty($item->name) ? $item->name : '';?></a>
			<?php
				endforeach;
			endif;
			?>
		</div>
	</div>
</div>    