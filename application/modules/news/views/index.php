<!-- Start popular-course Area -->
<section class="popular-course-area section-gap">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<?php echo Modules::run('news/news_menu', $categoryId) ?>
			</div> 
			<div class="col-lg-8">
				
				<div class="row">
				<?php
				if(!empty($news)):
					foreach($news as $keys => $item):
				?>
					<div class="col-md-12 ">
						<div class="single-popular-carusel">
							<div class="row course-2">
								<div class="thumb-wrap relative col-md-5 ">
									<div class="thumb relative">
										<div class="overlay overlay-bg"></div>	
										<img class="img-fluid" src="<?php echo !empty($item->image) ? $item->image :'';?>" alt="">
									</div>
									<div class="meta d-flex justify-content-between">
										<p>
											<span class="lnr lnr-users"></span> <?php echo !empty($item->view) ? number_format($item->view): 0;?> 
											<span class="lnr lnr-bubble"></span>0
										</p>
										
									</div>									
								</div>
								<div class="details col-md-7">
									<a href="<?php echo !empty($item->slug) ? base_url('news/detail/'.$item->slug) :'#';?>">
										<h4 style="color: #8a8a8a;">
											<?php echo !empty($item->title) ? $item->title: '';?>
										</h4>
										<p><?php echo !empty($item->createDate) ? $item->createDate :'';?> | by <?php echo !empty($item->userCreate->firstname) ? $item->userCreate->firstname:'';?> <?php echo !empty($item->userCreate->lastname) ? $item->userCreate->lastname:'';?></p>
									</a>
									<p style="padding-right: 8%;">
										<?php echo !empty($item->excerpt) ? $item->excerpt: '';?>
									</p>
								</div>
							</div>	
						</div>
					</div> 
				<?php
					endforeach;
				endif;
				?> 
				</div>
			</div>  
		</div>
	</div>	
</section>
<!-- End popular-course Area -->
