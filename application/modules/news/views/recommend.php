<div class="single-sidebar-widget popular-post-widget">
    <h4 class="popular-title">Popular Posts</h4>
    <div class="popular-post-list">
        <?php 
        if(!empty($news_list)):
            foreach($news_list as $list):
        ?>
        <div class="single-post-list d-flex flex-row align-items-center">
            <div class="thumb">
                <img class="img-fluid" src="<?php echo !empty($list->image) ? $list->image : '';?>" alt="" style="width: 100px;">
            </div>
            <div class="details">
                <a href="<?php echo !empty($list->slug) ? base_url('news/detail/'.$list->slug) :'#';?>"><h6><?php echo !empty($list->title) ? $list->title :'';?></h6></a>
                <p><?php echo !empty($list->createDate) ? $list->createDate :'';?></p>
            </div>
        </div>
        <?php
            endforeach;
        endif;
        ?> 										
    </div>
</div>