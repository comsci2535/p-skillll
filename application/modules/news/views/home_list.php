
<?php 
if(!empty($news_list)):
    foreach($news_list as $list):
?>
<div class="col-lg-3 col-md-6 single-blog">
    <div class="thumb">
        <img class="img-fluid" src="<?php echo !empty($list->image) ? $list->image : '';?>" alt="">               
    </div>
    <p class="meta"><?php echo !empty($list->createDate) ? $list->createDate :'';?> | <?php echo !empty($list->view) ? $list->view : 0;?> view</p>
        <a href="<?php echo !empty($list->slug) ? base_url('news/detail/'.$list->slug) :'#';?>">
            <h5><?php echo !empty($list->title) ? $list->title :'';?></h5>
        </a>
    <p>
        <?php echo !empty($list->excerpt) ? $list->excerpt :'';?>
    </p>
    <a href="<?php echo !empty($list->slug) ? base_url('news/detail/'.$list->slug) :'#';?>" class="details-btn d-flex justify-content-center align-items-center">
        <span class="details">Details</span><span class="lnr lnr-arrow-right"></span>
    </a>    
</div>
<?php
    endforeach;
endif;
?>                   