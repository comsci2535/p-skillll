<div class="single-sidebar-widget post-category-widget">
	<h4 class="category-title">Post Catgories</h4>
	<ul class="cat-list">
		<?php
		
		if(!empty($news)):
			foreach($news as $item):
		?>
		<li>
			<a href="<?php echo !empty($item->slug) ? base_url('news/'.$item->slug) : '';?>" class="d-flex justify-content-between">
				<p><?php echo !empty($item->name) ? $item->name : '';?></p>
				<p><?php echo !empty($item->count) ? $item->count : 0;?></p>
			</a>
		</li>	
		<?php
			endforeach;
		endif;
		?>														
	</ul>
</div>			