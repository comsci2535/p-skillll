<section class="post-content-area single-post-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 posts-list">
                <div class="single-post row">
                    <div class="col-lg-12">
                        <div class="feature-img">
                            <img class="img-fluid" src="<?php echo !empty($news->image) ? $news->image:'';?>" alt="">
                        </div>									
                    </div>
                    <div class="col-lg-3  col-md-3 meta-details">
                        <ul class="tags">
                            <li><a href="javascript:void(0)"><?php echo !empty($news->name) ? $news->name:'';?></a></li>
                        </ul>
                        <div class="user-details row">
                            <p class="user-name col-lg-12 col-md-12 col-6">
                                <a href="javascript:void(0)"><?php echo !empty($news->userCreate->firstname) ? $news->userCreate->firstname:'';?> <?php echo !empty($news->userCreate->lastname) ? $news->userCreate->lastname:'';?></a> 
                                <span class="lnr lnr-user"></span>
                            </p>
                            <p class="date col-lg-12 col-md-12 col-6">
                                <a href="javascript:void(0)"><?php echo !empty($news->createDate) ? $news->createDate:'';?></a> <span class="lnr lnr-calendar-full"></span>
                            </p>
                            <p class="view col-lg-12 col-md-12 col-6"><a href="javascript:void(0)"><?php echo !empty($news->view) ? $news->view:0;?> Views</a> <span class="lnr lnr-eye"></span></p>
                            <!-- <p class="comments col-lg-12 col-md-12 col-6"><a href="javascript:void(0)">06 Comments</a> <span class="lnr lnr-bubble"></span></p> -->
                            <?php echo Modules::run('social/share_button'); ?>
                            <!-- <ul class="social-links col-lg-12 col-md-12 col-6">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-github"></i></a></li>
                                <li><a href="#"><i class="fa fa-behance"></i></a></li>
                            </ul>																				 -->
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <h3 class="mt-20 mb-20">
                            <?php echo !empty($news->title) ? $news->title :'';?>
                        </h3>
                        <p class="excert">
                           <?php echo !empty($news->excerpt) ? $news->excerpt :'';?>
                        </p>
                    </div>
                    <div class="col-lg-12">
                        <div class="quotes">
                            <?php echo !empty($news->detail) ? html_entity_decode($news->detail) :'';?>							
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="widget-wrap">
                    <?php echo Modules::run('news/recommend');?>
                    <!-- get category -->
                    <?php echo Modules::run('news/news_category');?>		
                </div>
            </div>
        </div>
    </div>	
</section>