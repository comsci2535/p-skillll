<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class News extends MX_Controller {
    
    private $_grpContent = "news";

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('news_m');
        $this->load->model("admin/upload_m");
        $this->load->model('user/user_m');
    }

    public function index($id = 0) {
        $this->session->unset_userdata('course_data');
        
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('news/').$id;
        $this->session->set_userdata('urlreffer', $urlreffer);
        if(!empty($id)):
            $inputcate['slug']      = $id;
            $category               = $this->news_m->get_rows($inputcate)->row();  
            $data['categoryId']     = !empty($category->categoryId) ? $category->categoryId : 0;
        endif;
        $input['active']        = '1';
        $input['recycle']       = '0';
        $input['slugs']          = $id;
        $input['grpContent']    = $this->_grpContent;
        $info                   = $this->news_m->get_news_rows($input)->result();
        
        if(!empty($info)):
            foreach($info as $item):
                $input_u['grpContent']  = $this->_grpContent;
                $input_u['contentId']   = $item->contentId;
                $file_ = $this->news_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)):
                    $item->image = base_url($file_->path.$file_->filename); 
                endif;
                $item->createDate       = date_language($item->createDate, true, 'th');
                $input_['userId']       = $item->createBy;
                $item->userCreate       = $this->user_m->get_rows($input_)->row();
            endforeach;
        endif;
        
        $data['news']               = $info;
        
        $data['course_act']         = 'active';
        $data['contentView']        = "news/index";
        $data['pageHeader']         = $info->metaTitle;
        $data['metaTitle']          = $info->metaTitle;
        $data['metaDescription_']   = $info->metaDescription;
        $data['metaKeyword_']       = $info->metaKeyword;

        $share['ogTitle']           = $info->metaTitle;
        $share['ogDescription']     = $info->metaDescription;
        $share['ogUrl']             = 'news/detail/'.$id;
        $this->_social_share($share);
        $this->front->layout_blog($data);
    }

    public function home_news($id=0)
    {
        $input['categoryType']  = $this->_grpContent;
        $info                   = $this->news_m->get_news_map_rows($input)->result();
        $data['news']           = $info;
        $this->load->view('home_news', $data);
    }

    public function news_category($id=0)
    {
        $input['categoryType']  = $this->_grpContent;
        $input['active']        ='1';
        $input['recycle']       ='0';
        $info                   = $this->news_m->get_news_map_rows($input)->result();
        if(!empty($info)):
            foreach($info as $item):
                $input_['active']        ='1';
                $input_['recycle']       ='0';
                $input_['categoryId']    = $item->categoryId;
                $input_['categoryType']  = $this->_grpContent;
                $item->count = $this->news_m->get_count($input_);
            endforeach;
        endif;
        $data['news']           = $info;
        $this->load->view('news_category', $data);
    }

    public function news_menu($id=0)
    { 
        $input['categoryType']  = $this->_grpContent;
        $input['active']        ='1';
        $input['recycle']       ='0';
        $info                   = $this->news_m->get_news_map_rows($input)->result();
        $data['category']       = $info;
        $data['categoryId']     = $id;
        $this->load->view('news_menu', $data);
    }

    public function home_list($id=0)
    {
        $input['grpContent']    = $this->_grpContent;
        $input['active']        ='1';
        $input['recycle']       ='0';
        $input['length']        = 8;
        $input['start']         = 0; 
        $info                   = $this->news_m->get_news_rows($input)->result();

        if(!empty($info)){
            foreach($info as $item){
                $input_u['grpContent']  = $this->_grpContent;
                $input_u['contentId']   = $item->contentId;
                $file_ = $this->news_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                    $item->image = base_url($file_->path.$file_->filename); 
                } 
                $item->createDate = date_language($item->createDate, true, 'th');
            } 
        } 
        $data['news_list']      = $info;
        $this->load->view('home_list', $data);
    }

    public function recommend($id=0)
    {
        $input['grpContent']    = $this->_grpContent;
        $input['active']        ='1';
        $input['recommend']     = 1;
        $input['recycle']       ='0';
        $input['length']        = 4;
        $input['start']         = 0; 
        $info                   = $this->news_m->get_news_rows($input)->result();
        if(!empty($info)){
            foreach($info as $item){
                $input_u['grpContent']  = $this->_grpContent;
                $input_u['contentId']   = $item->contentId;
                $file_ = $this->news_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                    $item->image = base_url($file_->path.$file_->filename); 
                } 
                $item->createDate = date_language($item->createDate, true, 'th');
            } 
        } 
        $data['news_list']      = $info;
        $this->load->view('recommend', $data);
    }

    public function ajax_news_data()
    {
        $input['grpContent']    = $this->_grpContent;
        $input['active']        ='1';
        $input['recycle']       ='0';
        $input['length']        = 8;
        $input['start']         = 0;
        $categoryId             = $this->input->post('categoryId');
        if($categoryId !='all'):
            $input['categoryId'] = $categoryId;  
        endif; 
        $info                = $this->news_m->get_news_rows($input)->result(); 
        if(!empty($info)){
            foreach($info as $item){
                $input_u['grpContent']  = $this->_grpContent;
                $input_u['contentId']   = $item->contentId;
                $file_ = $this->news_m->get_uplode($input_u)->row();
                if (!empty($file_) && is_file($file_->path.$file_->filename)) {
                    $item->image = base_url($file_->path.$file_->filename); 
                } 
                $item->createDate = date_language($item->createDate, true, 'th');
            } 
        } 
        $data['news_list']      = $info;
        $result                 = $this->load->view('home_news_data', $data); 
        json_encode($result);
    }

    public function detail($id=0)
    {
        
        $this->session->unset_userdata('course_data');
        
        $this->load->module('front');

        $this->session->unset_userdata('urlreffer');
        $urlreffer['url'] = site_url('news/detail/').$id;
        $this->session->set_userdata('urlreffer', $urlreffer);
        
        $input['active']        = '1';
        $input['recycle']       = '0';
        $input['slug']          = $id;
        $input['grpContent']    = $this->_grpContent;
        $info                   = $this->news_m->get_news_rows($input)->row();
        $info->image            = "";
        if(!empty($info)):
            $input_u['grpContent']  = $this->_grpContent;
            $input_u['contentId']   = $info->contentId;
            $file_ = $this->news_m->get_uplode($input_u)->row();
            if (!empty($file_) && is_file($file_->path.$file_->filename)):
                $info->image = base_url($file_->path.$file_->filename); 
            endif;
            $info->createDate       = date_language($info->createDate, true, 'th');
            $input_['userId']       = $info->createBy;
            $info->userCreate       = $this->user_m->get_rows($input_)->row();

        endif;

        $data['news']               = $info;
        
        $data['course_act']         = 'active';
        $data['contentView']        = "news/detail";
        $data['pageHeader']         = $info->metaTitle;
        $data['metaTitle']          = $info->metaTitle;
        $data['metaDescription_']   = $info->metaDescription;
        $data['metaKeyword_']       = $info->metaKeyword;

        $share['ogTitle']           = $info->metaTitle;
        $share['ogDescription']     = $info->metaDescription;
        $share['ogUrl']             = 'news/detail/'.$id;
        $share['ogImage']           = $info->image;
        $this->_social_share($share);
        $this->front->layout_blog($data);
    }

    public function _social_share($input)
    {
        $param['ogType']        = "webiste";
        $param['ogUrl']         = $input['ogUrl'];
        $param['ogTitle']       = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage']       = $input['ogImage'];
        $param['twImage']       = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }
}
