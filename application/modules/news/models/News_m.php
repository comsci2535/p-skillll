<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class News_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.*')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->from('category a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.categoryId')
                        ->from('category a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }

     public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('category a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->where('a.categoryId', $id)
                        ->get()
                        ->row();
        return $query; 
    }

    private function _condition($param) 
    {
        
        //$this->db->where('b.lang', $this->curLang);
        
        if ( isset($param['categoryType']))
                $this->db->where('a.type', $param['categoryType']);
        
        $this->db->order_by('a.parentId', 'asc');
        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.order";
                $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        }
        
        if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);
        
        if ( isset($param['categoryId']) ) 
            $this->db->where('a.categoryId', $param['categoryId']);

        if ( isset($param['slug']) ) 
            $this->db->where('b.slug', $param['slug']);

        //if ( isset($param['recycle']) )
            $this->db->where('a.recycle', 0);
        
         //if ( isset($param['active']) )
            $this->db->where('a.active', 1);
    }

    public function get_news_map_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.*')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->join('content c', 'b.categoryId = c.categoryId')
                        ->from('category a')
                        ->group_by('b.categoryId')
                        ->get();
        return $query;
    }

    public function get_news_rows($param) 
    {
        $this->_news_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*,b.name')
                         ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query;
    }


    public function get_news_count($param) 
    { 
        $this->_news_condition($param);
        $query = $this->db
                        ->select('a.*,b.name')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }


    private function _news_condition($param) 
    {
        $this->db->where('a.grpContent', $param['grpContent']);


        if ( isset($param['keyword']) ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }

       if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updateDate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }    

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.view";
            if ($param['order'][0]['column'] == 3) $columnOrder = "b.name";
            if ($param['order'][0]['column'] == 4) $columnOrder = "a.createDate";
            if ($param['order'][0]['column'] == 5) $columnOrder = "a.updateDate";
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        }

        if (isset($param['categoryId']) && $param['categoryId']!="") 
            $this->db->where('a.categoryId', $param['categoryId']);

        if ( isset($param['title']) ) 
            $this->db->where('a.title', $param['title']);

        if ( isset($param['slug']) ) 
            $this->db->where('a.slug', $param['slug']);
        
        if ( isset($param['slugs']) ) 
            $this->db->where('b.slug', $param['slugs']);
            
        if ( isset($param['contentId']) ) 
             $this->db->where('a.contentId', $param['contentId']);

        if ( !in_array($this->router->method, array("profile","check_password","check_email")))
            $this->db->where('a.contentId !=', $this->session->content['contentId']);

        if ( isset($param['recycle']) ){
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            } 
        } 

    }

    public function get_uplode($param) 
    {
        $this->_condition_uplode($param);
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('upload a')
                        ->join('upload_content b', 'a.uploadId = b.uploadId', 'left')
                        ->get();
        return $query;
    }

    private function _condition_uplode($param) 
    {

        $this->db->where('a.grpContent', $param['grpContent']);
        if ( isset($param['contentId']) ) 
             $this->db->where('b.contentId', $param['contentId']);

        if ( isset($param['grpType']) ) 
             $this->db->where('b.grpType', $param['grpType']);

    }
    
}
