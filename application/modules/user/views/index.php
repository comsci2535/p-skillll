<div class="container-fluid">
    <div class="col-xs-12 col-md-4 col-md-offset-4 page-member">
        <?php echo form_open($frmAction, 'class="frm-login"'); ?>
        <div class="col-xs-12"><input value="" type="email" name="email" class="form-control" placeholder="<?php echo lang('email'); ?>" required></div>	
        <div class="col-xs-12"><input value="" type="password" name="password" class="form-control" placeholder="<?php echo lang('password'); ?>" required></div>	
        <div class="col-xs-12"style="padding-top:20px;"><button class="btn" type="submit"><?php echo lang('login'); ?></button></div>
        <div class="col-xs-12"><div id="result" style="padding-top:20px;"></div></div>
            <div class="col-xs-12 link-register">
                <a href="<?php echo site_url('register'); ?>">สมัครสมาชิก</a>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url('member/forgot'); ?>">ลืมรหัสผ่าน</a>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>
