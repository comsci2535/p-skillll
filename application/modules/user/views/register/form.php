<section class="section post-content-area single-post-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="blog-register">
					<div class="row title">
						<div class="col-md-12">
							<h3>สมัครสมาชิก<hr></h3>
						</div>
						<!-- <div class="separator"></div> -->
					</div> 
					<?php echo form_open($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post','autocomplete'=>'off')) ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="username">ชื่อผู้ใช้งาน/Username: <span class="required">*</span></label>
								<input type="text" class="form-control field-long" id="username" name="username">
								<div class="alert_username"></div>
							</div>

							<div class="form-group">
								<label for="password">รหัสผ่าน: <span class="required">*</span></label>
								<input type="password" class="form-control field-long" id="password" name="password">
								<div class="alert_password"></div>
							</div>

							<div class="form-group">
								<label for="passwordConf">ยืนยันรหัสผ่าน: <span class="required">*</span></label>
								<input type="password" class="form-control field-long" id="passwordConf" name="passwordConf">
								<div class="alert_passwordConf"></div>
							</div>

							<div class="form-group">
								<label for="fname">ชื่อ: <span class="required">*</span></label>
								<input type="text" class="form-control field-divided" id="fname" name="fname" placeholder="ชื่อ">
								<div class="alert_name"></div>
							</div>

							<div class="form-group">
								<label for="lname">นามสกุล: <span class="required">*</span></label>
								<input type="text" class="form-control field-divided" id="lname" name="lname" placeholder="นามสกุล">
								<div class="alert_name"></div>
							</div>
							<div class="form-group">
								<label for="email">Email: </label>
								<input type="email" class="form-control field-long" id="email" name="email" placeholder="Email"> 
							</div>

							<div class="form-group">
								<label for="email">เบอร์โทรศัพท์: </label>
								<input type="text" class="form-control field-long" id="tel" name="tel"> 
							</div>

							<div class="form-group">
								<input type="hidden" name="mode" value="create" />
								<div id="html_element" style="margin-bottom: 10px"></div>
								<input type="hidden" name="robot"  class="form-control">
								<div id="form-success-div" class="text-success"></div> 
								<div id="form-error-div" class="text-danger"></div>
							</div>
							<div class="form-group">
								<span id="form-img-div"></span> 
								<button id="form-submit-div" type="submit" class="btn btn-flat btn-register button ">สมัครสมาชิก</button>
							</div> 
						</div>     
							
						
					</div><!-- row -->
					<?php echo form_close() ?>
				</div><!-- blog-posts -->
			</div><!-- col-lg-4 -->

			<div class="col-lg-3 col-md-3">
			</div>
		</div><!-- row -->
	</div><!-- container -->
</section><!-- section -->