<style type="text/css">
	.span-d{
		cursor: pointer;
    color: #eceeef;
    text-align: center;
    padding: 0px 8px;
    text-decoration: none;
    /* font-size: 14px; */
    margin: 2px;
    border-radius: 25px;
    background-color: #ff0000;
	}
</style>
<section class="section post-content-area single-post-area">
	<div class="container">
		<div class="row">
			<div class=" col-sm-12 text-center">
                <span class="topnav-article">
                  <a class="active"  href="<?php echo site_url("user/profile");?>">ข้อมูลส่วนตัว</a>
                  <!-- <a  href="<?php echo site_url("user/profile/deposit");?>">เงินสะสม/ประวัติการถอน</a> -->
                 <!--  <a   href="<?php echo site_url("user/profile/deposit");?>">ประวัติการถอน</a> -->
                </span>
            </div>
			<div class="col-lg-3 col-md-3">
			</div>
			<div class="col-lg-6 col-md-6">
				<div class="blog-register"> 
					<?php echo form_open($frmAction, array('class' => '', 'id'=>'frm-save' , 'method' => 'post','autocomplete'=>'off')) ?>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<div class="img-profile" style="text-align: center;padding: 20px;">
								<img alt="K SME" src="<?php echo $this->session->member['image'];?>">
								</div>
							</div>
							<div class="form-group">
								<label for="couponCode" style="cursor: pointer;" data-toggle="tooltip" data-placement="bottom"
									title="" data-original-title="รหัสคูปองส่วนลด 200 บาท สำหรับเพื่อนของคุณ หากใครใช้รหัสนี้ในการสมัครคอร์ส คุณจะได้รับทันที 20% จากค่าสมัคร"
									class="red-tooltip">รหัสสมาชิก: 
									<span data-toggle="tooltip" 
									data-placement="bottom"
									title="" 
									data-original-title="รหัสคูปองส่วนลด 200 บาท สำหรับเพื่อนของคุณ หากใครใช้รหัสนี้ในการสมัครคอร์ส คุณจะได้รับทันที 20% จากค่าสมัคร"
									class="red-tooltip span-d" > 
									?</span></label>
								<input type="text" class="form-control field-long" id="username" name="username" value="<?php echo $profile->couponCode; ?>" disabled>
								<div class="alert_username"></div>
							</div> 
							<?php if($profile->oauth_provider!="facebook"){ ?>  
							<div class="form-group">
								<label for="username">ชื่อผู้ใช้งาน/Username: </label>
								<input type="text" class="form-control field-long" id="username" name="username" value="<?php echo $profile->username; ?>" disabled>
								<div class="alert_username"></div>
							</div>
							<div class="form-group">
								<label for="passwordOld">รหัสผ่านใหม่: </label>
								<input type="text" class="form-control field-long" id="passwordOld" name="passwordOld">
								<div class="alert_passwordOld"></div>
							</div>
							<div class="form-group">
								<label for="password">รหัสผ่านเดิม: </label>
								<input type="password" class="form-control field-long" id="password" name="password">
								<div class="alert_password"></div>
							</div>
							<div class="form-group">
								<label for="passwordConf">ยืนยันรหัสผ่าน: </label>
								<input type="password" class="form-control field-long" id="passwordConf" name="passwordConf">
								<div class="alert_passwordConf"></div> 
							</div>

							<input type="hidden" name="ff" id="ff" value="1" />
							<?php }else{ ?>
							<input type="hidden" name="ff" id="ff" value="" />
							<?php } ?>

							<div class="form-group">
								<label for="fname">ชื่อ: <span class="required">*</span></label>
								<input type="text" class="form-control field-divided" id="fname" name="fname" value="<?php echo $profile->firstname; ?>" placeholder="ชื่อ">
								<div class="alert_name"></div>
							</div>

							<div class="form-group">
								<label for="lname">นามสกุล: <span class="required">*</span></label>
								<input type="text" class="form-control field-divided" id="lname" name="lname" value="<?php echo $profile->lastname; ?>" placeholder="นามสกุล">
								<div class="alert_name"></div>
							</div>

							<div class="form-group">
								<label for="email">Email: </label>
								<input type="email" class="form-control field-long" id="email" name="email"  value="<?php echo $profile->email; ?>" placeholder="Email"> 
							</div>

							<div class="form-group">
								<label for="email">เบอร์โทรศัพท์: </label>
								<input type="text" class="form-control field-long" id="tel" name="tel" value="<?php echo $profile->phone; ?>"> 
							</div>
							<div class="form-group">
								<input type="hidden" name="userId" id="userId" value="<?php echo $profile->userId; ?>" />
								<input type="hidden" name="mode" value="create" />
								<div id="html_element" style="margin-bottom: 10px"></div>
								<input type="hidden" name="robot"  class="form-control">
								<div id="form-success-div" class="text-success"></div> 
								<div id="form-error-div" class="text-danger"></div>
								<span id="form-img-div"></span>
								<button type="submit" class="btn btn-flat btn-register button"><span id="form-img-div"></span> แก้ไขข้อมูล</button> 
							</div> 
						</div> 
					</div><!-- row -->
					<?php echo form_close() ?>
				</div><!-- blog-posts -->
			</div><!-- col-lg-4 -->

			<div class="col-lg-3 col-md-3">
			</div>
		</div><!-- row -->
	</div><!-- container -->
</section><!-- section -->