<div class="row">
    <h1>Modules:- <?php echo $this->router->class; ?></h1>

    <hr>
    <a href="javascript:;" id="fbLogin">FB login</a>
    <div id="status"></div>

    <hr>
    <a href="javascript:;" id="ggLogin">Google+ login</a>
    <div id="name"></div>

    <hr>

    <?php echo form_open($frmAction, "class='frm-login'") ?>

    <div class="form-group">
        <label><?php echo lang('email'); ?></label>
        <input type="email" class="form-control" name="email" id="email" value="" required placeholder="<?php echo lang('email'); ?>">
    </div>


    <div class="form-group">
        <label><?php echo lang('password'); ?></label>
        <input type="password" class="form-control" name="password" id="password" value="" required placeholder="<?php echo lang('password'); ?>">
    </div>


    <div id="login-result"></div>

    <hr>

    <a href="<?php echo site_url("forgot-password"); ?>" id="ggLogin"><?php echo lang('forgotPassword'); ?></a>

    <hr>


    <input type="submit" value="Submit">
    <?php echo form_close(); ?>

</div>

