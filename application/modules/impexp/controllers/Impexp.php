<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Impexp extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("impexp_m");
    }

    public function index() {
    	$data['pageHeader']='นำเข้า-ส่งออก';
        $this->load->module('front');
        $data['item']=$this->impexp_m->getRows();
        // print"<pre>";
        // print_r($data['item']);
        // exit();
        
        $data['contentView'] = 'impexp/index';
        $this->front->layout($data);
    }
    public function get_quantity() {
        
        $data['quantity']=$this->impexp_m->getQuantityOverviewReport();
        
        $data['quantity_start_y'] = intval($data['quantity'][0]->YR) ;
        $quantity_imp = [] ;
        $quantity_exp = [] ;
        foreach ($data['quantity'] as $key => $value) {
            if($value->IMPEXP=='นำเข้า'){
                $quantity_imp[]=intval($value->A/1000000);
            }
            if($value->IMPEXP=='ส่งออก'){
                $quantity_exp[]=intval($value->A/1000000);
            }
        }
        
        $data['quantity_imp']=$quantity_imp;
        $data['quantity_exp']=$quantity_exp;
       $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    public function get_value() {
        
       $data['value']=$this->impexp_m->getValueOverviewReport();
        
        $data['value_start_y'] = intval($data['value'][0]->YR) ;
        $value_imp = [] ;
        $value_exp = [] ;
        foreach ($data['value'] as $key => $value) {
            if($value->IMPEXP=='นำเข้า'){
                $value_imp[]=intval($value->B/1000000);
            }
            if($value->IMPEXP=='ส่งออก'){
                $value_exp[]=intval($value->B/1000000);
            }
        }
        
        $data['value_imp']=$value_imp;
        $data['value_exp']=$value_exp;
       $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

}
