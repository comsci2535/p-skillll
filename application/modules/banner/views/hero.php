
<!-- 
<div class="main-slider" id="slider-c">
  <div id="owl-example" class="owl-carousel owl-theme">
    <?php foreach ($info as $rs) :?>
    
    <div class="item ">
       <?php if ($rs->link){ ?>
          <a href="<?php echo $rs->link ?>" >
           <div class="banner-pc">
              <img src="<?php echo $rs->image['coverImage'] ?>" class="img-responsive">
           </div>
           <div class="banner-mobile">
                <img src="<?php echo $rs->image['contentImage'] ?>" class="img-responsive">
          </div>
          </a>
        <?php }else{ ?>
           <div class="banner-pc">
              <img src="<?php echo $rs->image['coverImage'] ?>" class="img-responsive">
           </div>
           <div class="banner-mobile">
                <img src="<?php echo $rs->image['contentImage'] ?>" class="img-responsive">
          </div>
        <?php } ?>
    </div>
    
   
    <?php endforeach; ?>
  </div> 
</div> -->


<!-- <div class="marketing-banner hero-top before-header">
  <?php foreach ($info as $rs) :?>
    <img src="<?php echo $rs->image['coverImage'] ?>" alt="" class="hero-placeholder" />
    <div class="center-wrapper">
        <div class="centered content">
            <h1 class="jumbotron">Tomorrow is for the Taking</h1>
            <h2>Thousands of classes to fuel your creativity and career.</h2>
             <?php if ($rs->link){ ?>
            <a href="<?php echo $rs->link ?>" class="button large wide free-signup-button" >อ่านต่อ</a>
            <?php } ?>
        </div>
    </div>
  <?php endforeach; ?>
</div> -->

<?php foreach ($info as $rs) :?>
  <!-- start banner Area -->
  <section class="banner-area relative" id="home" style="background: url(<?php echo $rs->image['coverImage'] ?>) right; background-size: cover;">
   <!--  <div class="overlay overlay-bg"></div>   -->
     
   
    <div class="container" >
      <div class="row fullscreen d-flex align-items-center justify-content-between">
        <div class="banner-content col-lg-12 col-md-12">
          <h1 class="text-uppercase">
            We Ensure better education
            for a better world      
          </h1>
          <p class="pt-10 pb-10">
            In the history of modern astronomy, there is probably no one greater leap forward than the building and launch of the space telescope known as the Hubble.
          </p>
          <a href="#" class="primary-btn text-uppercase">Get Started</a>
        </div>                    
      </div>
    </div> 
   
  </section>
  <?php endforeach; ?> 

      <!-- End banner Area -->