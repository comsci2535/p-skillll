<h4><?php echo count($content);?> Lessons <!-- (57m) --></h4>
<div class="body-link-vdo">
	<ul style="height:390px;overflow-y: scroll;">
		<?php foreach ($content as $key => $rs) { ?>
			<h5><?php echo $rs->title ?></h5> 
			<?php if(!empty($rs->parent)){ foreach ($rs->parent as $key_ => $rs_) { ?> 
				<?php if($rs_->type==0){ ?>
				<li class="item  ">
					<div class="media">
						<div class="bg-circle">
							<div id="circle-" class="circle-none"></div>
						</div>
						<div class="media-body col-xs-8 col-sm-8 col-md-8 col-8">
							<a href="javascript:void(0)" id="vimeo-" class="vimeo-b" >
								<span class="color5 title fontLv8"><?php echo $rs_->title ?></span>
								
							</a>
						</div>
						<div class="media-body col-xs-3 col-sm-3 col-md-3 col-3">
							<span class="clock"><i class="fa fa-clock-o"></i> <?php echo $rs_->videoLength ?> น.</span>
						</div>
					</div>  
				</li>
			    <?php }else{ ?>
			    <li class="item  ">
					<div class="media">
						<div class="bg-circle">
							<img alt="" src="<?php echo base_url('assets/images/lock.png'); ?>" style="width:22px;"/>
						</div>
						<div class="media-body col-xs-8 col-sm-8 col-md-8 col-8">
							<a href="javascript:void(0)" id="vimeo-" class="vimeo-b" >
								<span class="color5 title fontLv8"><?php echo $rs_->title ?></span>
								
							</a>
						</div>
						<div class="media-body col-xs-3 col-sm-3 col-md-3 col-3">
							<span class="clock"><i class="fa fa-clock-o"></i> <?php echo $rs_->videoLength ?> น.</span>
						</div>
					</div>  
				</li>
			    <?php } ?>
		    <?php } }?>
		<?php } ?>
		<!-- <li class="item ">
			<div class="media">
				<div class="bg-circle">
					<div id="circle-" class="circle-none"></div>
				</div>
				<div class="media-body col-xs-8 col-sm-8 col-md-8 col-8">
					<a href="javascript:void(0)" id="vimeo-" class="vimeo-b" >
						<span class="color5 title fontLv8">ttttt</span>
						
					</a>
				</div>
				<div class="media-body col-xs-3 col-sm-3 col-md-3 col-3">
					<span class="clock"><i class="fa fa-clock-o"></i> 12:00 น.</span>
				</div>
			</div>  
		</li>
		<li class="item ">
			<div class="media">
				<div class="bg-circle">
					<div id="circle-" class="circle-none"></div>
				</div>
				<div class="media-body col-xs-8 col-sm-8 col-md-8 col-8">
					<a href="javascript:void(0)" id="vimeo-" class="vimeo-b" >
						<span class="color5 title fontLv8">ttttt</span>
						
					</a>
				</div>
				<div class="media-body col-xs-3 col-sm-3 col-md-3 col-3">
					<span class="clock"><i class="fa fa-clock-o"></i> 12:00 น.</span>
				</div>
			</div>  
		</li> -->
	</ul>               
</div>