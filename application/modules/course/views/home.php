
<?php  
if(!empty($course_home)):
	foreach($course_home as $item):
?>
<div class="col-md-3">
	<div class="single-popular-carusel">
		<div class="thumb-wrap relative">
			<div class="thumb relative">
				<a href="<?php echo !empty($item->slug) ? base_url('course/detail/'.$item->slug) :'#';?>">
					<div class="overlay overlay-bg"></div>  
					<img class="img-fluid" src="<?php echo !empty($item->image) ? $item->image :'';?>" alt="">
				</a>
			</div>
			<div class="meta d-flex justify-content-between">
				<p>
					<span class="lnr lnr-users"></span> <?php echo !empty($item->view) ? number_format($item->view): 0;?> 
					<span class="lnr lnr-bubble"></span>0
				</p>
				<h4><?php echo !empty($item->price) ? number_format($item->price): 0;?>฿</h4>
			</div>                  
		</div>
		<div class="details">
			<a href="<?php echo !empty($item->slug) ? base_url('course/detail/'.$item->slug) :'#';?>">
				<h4>
					<?php echo !empty($item->title) ? $item->title: '';?>
				</h4>
			</a>
			<p>
				<?php
				$instructor_name = '';
				if(!empty($item->instructor)):
					foreach($item->instructor as $key => $instructor):
						if($key > 0):
							$instructor_name.= ','.$instructor->firstname.' '.$instructor->lastname;
						else:
							$instructor_name.= $instructor->firstname.' '.$instructor->lastname;
						endif;
					endforeach;
				endif;
				echo $instructor_name;
				?>
			</p>
		</div> 
	</div>  
</div>
<?php
	endforeach;
endif;
?>