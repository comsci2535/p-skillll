<!-- Start popular-course Area -->
<section class="popular-course-area section-gap">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<?php echo Modules::run('category/course_menu', $categoryId) ?>
			</div> 
			<div class="col-lg-8">
				
				<div class="row">
				<?php
				// arr($course_home);
				if(!empty($course_home)):
					foreach($course_home as $keys => $item):
						if($keys == 0):
						?>
							<div class="col-md-12 ">
								<div class="single-popular-carusel">
									<div class="row course-2">
										<div class="thumb-wrap relative col-md-5 ">
											<div class="thumb relative">
												<div class="overlay overlay-bg"></div>	
												<img class="img-fluid" src="<?php echo !empty($item->image) ? $item->image :'';?>" alt="">
											</div>
											<div class="meta d-flex justify-content-between">
												<p>
													<span class="lnr lnr-users"></span> <?php echo !empty($item->view) ? number_format($item->view): 0;?> 
													<span class="lnr lnr-bubble"></span>0
												</p>
												<h4><?php echo !empty($item->price) ? number_format($item->price): 0;?>฿</h4>
											</div>									
										</div>
										<div class="details col-md-7">
											<a href="<?php echo !empty($item->slug) ? base_url('course/detail/'.$item->slug) :'#';?>">
												<h4 style="color: #8a8a8a;">
													<?php echo !empty($item->title) ? $item->title: '';?>
												</h4>
											</a>
											<p style="padding-right: 8%;">
												<?php echo !empty($item->excerpt) ? $item->excerpt: '';?>
												<?php
												$instructor_name = '';
												if(!empty($item->instructor)):
													foreach($item->instructor as $key => $instructor):
														if($key > 0):
															$instructor_name.= ','.$instructor->firstname.' '.$instructor->lastname;
														else:
															$instructor_name.= $instructor->firstname.' '.$instructor->lastname;
														endif;
													endforeach;
												endif;
												echo ' by '.$instructor_name;
												?>
											</p>
										</div>
									</div>	
								</div>
							</div> 
						<?php
						else:
						?> 
							<div class="col-md-4">
								<div class="single-popular-carusel">
									<div class="thumb-wrap relative">
										<div class="thumb relative">
											<div class="overlay overlay-bg"></div>	
											<img class="img-fluid" src="<?php echo !empty($item->image) ? $item->image :'';?>" alt="">
										</div>
										<div class="meta d-flex justify-content-between"> 
											<p>
												<span class="lnr lnr-users"></span> <?php echo !empty($item->view) ? number_format($item->view): 0;?> 
												<span class="lnr lnr-bubble"></span>0
											</p>
											<h4><?php echo !empty($item->price) ? number_format($item->price): 0;?>฿</h4>
										</div>									
									</div>
									<div class="details">
										<a href="<?php echo !empty($item->slug) ? base_url('course/detail/'.$item->slug) :'#';?>">
											<h4 style="color: #8a8a8a;">
												<?php echo !empty($item->title) ? $item->title: '';?>
											</h4>
										</a>
										<p>
											<?php echo !empty($item->excerpt) ? $item->excerpt: '';?>									
										</p>
									</div>
								</div>	
							</div>  
						<?php
						endif; 
					endforeach;
				endif;
				?> 
				</div>
			</div>  
		</div>
	</div>	
</section>
<!-- End popular-course Area -->
