<!-- <div class="top_site_main-2" >
        <div class="page-title-wrapper">
            <div class="banner-wrapper container">
                <div class="text-title">
                    <h2>คอร์สสอนสด</h2>
                    <h4>จับมือสอน ลงมือทำจริง</h4>   
                </div>      
            </div>
        </div>
</div> -->
<?php echo Modules::run('banner/hero','activity') ?>
<section class="section blog-article">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-ms-12">
                    <div class="blog-posts">
                        
                        <?php if(!empty($info)){ ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-ms-12 ">
                                <div class="content-activity">
                                    <h4 ><?php echo $info['title'] ?></h4>
                                    <br>
                                    <!-- <span class="post-time">โพสเมื่อ <?php echo date_language($info['createDate'],true,'th') ?></span>
                                    <p><?php echo $info['excerpt'] ?></p> -->

                                   
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-ms-12 ">
                                
                                <img src="<?php echo $info['image']; ?>" alt="Blog Image">
                                
                            </div>
                            <div class="blog-activity col-lg-12 col-md-12 col-ms-12 ">
                                <div class="content-activity">
                                   <!--  <span class="title"><?php echo $info['title'];?></span>-->
                                    <!-- <p></p>  -->
                                    <ul>
                                        <li><?php echo $info['excerpt'];?></li>
                                       
                                    </ul>
                                    <ul>
                                        <li>วัน : <?php echo $info['date'];?></li>
                                        <li>เวลา : <?php echo $info['time'];?></li>
                                        <li>สถานที่ : <?php echo $info['location'];?></li>
                                    </ul>
                                    <?php if($info['promotion']!=""){ ?>
                                    <ul>
                                        <li>ราคา : <span style="font-size:18px; text-decoration: line-through;text-decoration-color: red;"><?php echo number_format($info['price']);?> </span> บาท</li>
                                        <li>โปรโมชั่น : <?php echo number_format($info['promotion']);?> บาท</li>
                                    </ul>
                                    <?php }else{ ?>
                                    <ul>
                                        <li>ราคา : <?php echo number_format($info['price']);?> บาท</li>
                                        
                                    </ul>
                                   <?php } ?>

                                    
                                </div><!-- single-post -->
                                <div class="content-detail">
                                        <?php echo html_entity_decode($info['detail']); ?>
                                </div><!-- single-post -->
                                 <div class="socila-link">
                                   <!--  <div class="sharethis-inline-share-buttons"></div> -->
                                     <?php echo Modules::run('social/share_button'); ?>
                                </div>
                                <?php if(!empty($image_g)){ ?>

                                     <div class="col-xs-12">  
                                                <br>   
                                                <center><h3> <?php echo $info['title_gallery']; ?> </h3></center>
                                                <br>
                                                  <!-- <div id="gallery1"></div> -->

                                                  <div id="gallery" style="display:none;">
                                                     <?php foreach ($image_g as $key => $value) { ?>
                                                            <a href="#">
                                                            <img alt="gallery"
                                                                 src="<?php echo $value;?>"
                                                                 data-image="<?php echo $value;?>"
                                                                 data-description="This is a Lemon Slice"
                                                                 style="display:none">
                                                            </a>
                                                     <?php } ?>

                                                  </div>
                                    </div>   
                                 <?php } ?>
                               
                            </div>
                            
                        </div><!-- row -->
                        
                        <?php } ?>
                        
                       
                    </div><!-- blog-posts -->
                </div><!-- col-lg-4 -->
                <div class="clearfix"></div>
               
               
            </div>  
        </div>
    </section><!-- section -->