<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Repo_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        
        $this->orclDB = $this->load->database('orcl_db', TRUE);

       
    }

    public function get_rows() 
    {
       
        $this->orclDB->select('*');
        $this->orclDB->from('DIM_PDT_TYPE_ALL');
        //$this->orclDB->join('table1', 'table1.id = table2.id');
        //$this->orclDB->join('table1', 'table1.id = table3.id');
        $query = $this->orclDB->get();
         return $query->result();
    }

}
