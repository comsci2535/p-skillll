

<section class="section blog-article">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-12 col-ms-12">
                    <div class="blog-posts">
                        
                        <?php if(!empty($info)){ ?>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-ms-12 ">
                                <div class="content-activity">
                                    <h4 ><?php echo $info['title'] ?></h4>
                                     <p>หมวดหมู่ : <?php echo $info['name'] ?></p>
                                    <span class="post-time">โพสเมื่อ : <?php echo date_language($info['createDate'],true,'th') ?> อ่าน : <?php echo number_format($info['view']); ?> </span>
                                    <p><?php echo $info['excerpt'] ?></p>

                                    <div class="tags-area">
                                        <ul class="tags">
                                             <?php foreach ($info['tags'] as $tg => $tg_) { 
                                                 $linkTags = str_replace(" ","-",$tg_);
                                                 
                                            ?>
                                                
                                            <li><a href="<?php echo site_url("article/tags/{$linkTags}");?>" class="btn" href="#"><?php echo $tg_; ?></a></li>
                                           
                                            <?php } ?>
                                
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-ms-12 ">
                                
                                <img src="<?php echo $info['image']; ?>" alt="Blog Image">
                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-ms-12 ">
                                <div class="socila-link">
                                  <!--   <div class="sharethis-inline-share-buttons"></div> -->
                                      <?php echo Modules::run('social/share_button'); ?>
                                </div>
                                <div class="content-detail">
                                        <?php echo html_entity_decode($info['detail']); ?>
                                </div>
                                 <div class="socila-link">
                                    <!-- <div class="sharethis-inline-share-buttons"></div> -->
                                      <?php echo Modules::run('social/share_button'); ?>
                                </div>
                               <div style="padding-top: 20px;">
                                    <div class="fb-comments" data-href="<?php echo site_url('/article/detail/'.$title_link); ?>" data-numposts="5" ></div>
                                </div>
                                
                            </div>
                            
                        </div>
                        
                        <?php } ?>
                        
                       
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="bg-gray col-xl-4 col-lg-4 col-md-12 col-sm-12 ">
                    <div class="blog-withe">
                        <div class="row ">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">

                                <div class="fb-page" data-href="https://www.facebook.com/gorradesign" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/gorradesign" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/gorradesign">เรียน illustrator by อ.กอร่า</a></blockquote></div>
                                <div id="fb-root"></div>
                                <script>(function(d, s, id) {
                                  var js, fjs = d.getElementsByTagName(s)[0];
                                  if (d.getElementById(id)) return;
                                  js = d.createElement(s); js.id = id;
                                  js.src = 'https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.1&appId=274883366696443&autoLogAppEvents=1';
                                  fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            </div>
                           
                        </div>
                        <div class="row ">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                              <?php echo Modules::run('article/relate',$info['categoryId'],$info['contentId']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="image-wrapper">
                                <a href="https://gorradesign.com/course/detail/illustrator-Basic-Pro">
                                <img src="<?php echo  base_url("assets/website/images/300x200-v2.jpg"); ?>"  class="img-responsive">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="image-wrapper">
                                <!-- <img src="<?php //echo  base_url("assets/website/images/300x200-b.png"); ?>"  class="img-responsive"> -->
                                <script type="text/javascript">
var uri = 'https://imp.tradedoubler.com/imp?type(img)g(23430164)a(3006884)' + new String (Math.random()).substring (2, 11);
document.write('<a href="https://clk.tradedoubler.com/click?p=264350&a=3006884&g=23430164" target="_BLANK"><img src="'+uri+'" border=0></a>');
</script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
</section>
    <input type="hidden" name="linkId" id="linkId" value="<?php echo $title_link; ?>">