<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report_order extends MX_Controller {

    private $_title = "รายงานสรุปการสั่่งซื้อ";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับรายงานสรุปการสั่่งซื้อ";
    private $_grpContent = "report_order";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("report_order_m");
        $this->load->model("course/course_m");
        $this->load->model("course_m",'ca');
        $this->load->model("Instructor_m");
        $this->_status = array('รอการอนุมัติ','อนุมัติเรียบร้อย','ยกเลิก');
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $export = array(
            'excel' => site_url("admin/{$this->router->class}/excel"),
            'pdf' => site_url("admin/{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        // $action[2][] = action_add(site_url("admin/{$this->router->class}/create"));
        // $action[2][] = action_export_group($export);
        // $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        // $action[3][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        

        $input_u['active'] = 1;
        $input_u['recycle'] = 0;
        $courseExclude = $this->ca->get_rows($input_u)->result();
        $data['course']=array();
        $data['course']['']="ทั้งหมด";
        foreach ($courseExclude as $key => $value) {
            $data['course'][$value->courseId]=$value->title;
        }

        $instructor_['recycle'] = 0;
        $instructor_['active'] = 1;
        $instructor = $this->Instructor_m->get_rows($instructor_)->result();
        $data['instructor'][''] = "ทั้งหมด";
        foreach ($instructor as $key => $value) {
            $data['instructor'][$value->instructorId]=$value->title;
        }
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;

        if($this->session->user['type'] == 'instructor'){
            $input['userId']    = $this->session->user['userId'];
            $info       = $this->report_order_m->get_instructor_map_rows($input);
            $infoCount  = $this->report_order_m->get_instructor_map_count($input);
        }else{
            $info       = $this->report_order_m->get_rows($input);
            $infoCount  = $this->report_order_m->get_count($input);
        } 
        
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        $i = $input['start'] + 1;
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->course_registerId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            //$column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";

             switch ($rs->status) {
                case 0 : $style = 'btn-warning'; break; //ยังไม่ขาย
                case 1 : $style = 'btn-success'; break; 
                case 2 : $style = 'btn-danger'; break; //ขายแล้ว
                default: $style = 'btn-default'; break;
            } 
            $statusPicke ="<lable class='label bg-blue'>".$this->_status[$rs->status]."</lable>";

             if(!empty($rs->couponCode)){ 
                if($rs->type==2){
                    $rt=($rs->price*$rs->discount)/100;
                    $total=$rs->price-$rt;
                    $discount=$rs->discount."%";
                }else{
                    $total=$rs->price-$rs->discount;
                    $discount=number_format($rs->discount);
                }
            }else{
                $total=$rs->price;
                $discount="ไม่มีส่วนลด";
            }
           
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['no'] = $i;
            $column[$key]['code'] = $rs->code;
            $column[$key]['title'] = $rs->title;
            $column[$key]['member'] = $rs->firstname.' '.$rs->lastname; 
            $column[$key]['couponCode'] = $rs->couponCode; 
            $column[$key]['total'] = number_format($total);
            $column[$key]['active'] = $statusPicke;
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            //$column[$key]['action'] = "";//Modules::run('admin/utils/build_button_group', $action);
            $i++;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
}
