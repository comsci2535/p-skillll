<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {
    
    private $_title = 'แผงควบคุม';
    private $_pageExcerpt = 'แสดงรายการโดยรวมของระบบ';
    private $_grpContent = 'grpContent';
    
    public function __construct() {
        parent::__construct();
        $this->load->model("home_m");
        $this->load->model("report_website_m");
    }

    public function index() {
        $this->load->module('admin');
        
        if ( $this->session->firstTime )
            Modules::run('admin/utils/toastr', 'info', config_item('appName'), 'ยินดีต้อนรับ');   
        $data['ddYear'] = $this->report_website_m->get_year_dd();
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(base_url("admin/{$this->router->class}/create"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        $data['user'] = $this->db
                                ->select('a.userId')
                                ->from('user a')
                                ->where('a.active',1)
                                ->where('a.recycle',0)
                                ->get()->num_rows();

        $data['course_register'] = $this->db
                                ->select('a.course_registerId')
                                ->from('course_register a')
                                ->where('a.status !=',2)
                                ->where('a.recycle',0)
                                ->get()->num_rows();
        $data['course'] = $this->db
                                ->select('a.courseId')
                                ->from('course a')
                                ->where('a.active',1)
                                ->where('a.recycle',0)
                                ->get()->num_rows();

        $data['track'] = $this->db
                                ->select('a.id')
                                ->from('track a')
                                ->where('a.type','front')
                                ->group_by('sessionId')
                                ->get()->num_rows();
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
             
        $this->admin->layout($data);
    }

    public function volume() {
        $this->load->module('admin');
        
        if ( $this->session->firstTime )
            Modules::run('admin/utils/toastr', 'info', config_item('appName'), 'ยินดีต้อนรับ');   
        
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(base_url("admin/{$this->router->class}/create"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ปริมาณการผลิต", "javascript:;");
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/volume";
             
        $this->admin->layout($data);
    }    
}
