<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {
    
//    public $language = array("th" => "ไทย", "en" => "English");
    public $language = array("th" => "ไทย");
    public $curLang;

    public function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->load->model("login_m");
    }
    
    public function layout($data) {

        if (empty($this->session->user['name']) && get_cookie('adminId')){
            $info = $this->db
                        ->select('a.*')
                        ->from('user a')
                        ->where('a.userId',get_cookie('adminId'))
                        ->get()
                        ->row_array();
                        
            $image = base_url('uploads/user.png');
            $upload = Modules::run('admin/upload/get_upload', $info['userId'], 'user', 'coverImage');
            if ( $upload->num_rows() != 0 ) {
                $row = $upload->row();
                if ( is_file("{$row->path}/{$row->filename}") )
                    $image = base_url("{$row->path}{$row->filename}");
            }
            $data['error'] = false;
            $data['message'] = site_url("admin");
            $user['userId'] = $info['userId'];
            $user['name'] = $info['firstname']." ".$info['lastname'];
            $user['image'] = $image;
            $user['email'] = $info['email'];
            $user['type'] = $info['type'];
            $user['policyId'] = $info['policyId'];
            $user['isBackend'] = TRUE;
            $this->session->set_userdata('user', $user);
            $this->session->set_flashdata('firstTime', '1');
            $this->login_m->update_last_login();
        }

        if (ENVIRONMENT == 'development')
            $this->output->enable_profiler(FALSE);
        
        $this->breadcrumbs->push('แผงควบคุม', site_url("admin/dashboard"));
        if (isset($data['breadcrumb']))
            $this->_breadcrumbs($data['breadcrumb']);
        
        if (!isset($data['contentView']))
            $data['contentView'] = 'empty';
        $data['pageTitle'] = "Admin";
        if (isset($data['pageHeader'])) 
            $data['pageTitle'] = $data['pageTitle'] . " | " . $data['pageHeader'];
        
        if ( !isset($data['pageScript']) ) {
             $data['pageScript'] = $this->_page_script();
        } else {
            $data['pageScript'] = '<script src="' . base_url($data['pageScript']) . '?v='.rand(0,100).'"></script>';
        }


        
        
        $data['sidebar'] = modules::run('admin/sidebar/gen');
        $data['curLang'] = config_item('language_abbr');
        $data['language'] = $this->language;
        
        $this->load->view('admin/admin/layout', $data);
    }

    private function _page_script() {
        $file = $this->router->method;
        if ( in_array($this->router->method,array('create','edit')) ) 
            $file = 'form';
        $script = "assets/scripts/admin/{$this->router->class}/{$file}.js";
        if ( is_file($script) ) {
            $pageScript = '<script src="' . base_url($script) . '?v='.rand(0,100).'"></script>';
        } else {
            $pageScript = '<!-- page no script -->';
        }
        return $pageScript;
    }

    private function _set_config() {
        $query = Modules::run('config/get_config', 'general');
        $lang = config_item('language_abbr');
        $lang = strtoupper($lang);
        foreach ($query as $rs)
            $this->config->set_item($rs['variable'], $rs['value']);
        return true;
    }

    private function _breadcrumbs($breadcurmbs) {
        if ( empty($breadcurmbs) )
            return;
        foreach ($breadcurmbs as $rs) {
            $this->breadcrumbs->push($rs[0], $rs[1]);
        }
        return;
    }

}
