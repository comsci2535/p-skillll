<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends MX_Controller {
    
    public function __construct()
    {
        parent::__construct();
         $this->load->helper('cookie');
    }
    
    public function index()
    {
        $this->session->unset_userdata('user');
         delete_cookie('adminId');
         //$this->session->unset_userdata('member');
        redirect(site_url("admin/dashboard"));

    }
}
