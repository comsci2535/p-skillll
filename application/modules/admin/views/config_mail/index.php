<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">อีเมล์</h3>
    </div>
    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
        <h4 class="block">Administrator email</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >Default</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['mailDefault']) ? $info['mailDefault'] : NULL ?>" type="text" id="" class="form-control" name="mailDefault">
            </div>
        </div>                      
        <h4 class="block">Mail sever</h4>
        <div class="form-group hidden">
            <label class="col-sm-2 control-label" >Send method</label>
            <div class="col-sm-7 icheck-inline">
                <?php  $info['mailMethod'] = isset($info['mailMethod']) ? $info['mailMethod'] : 1 ?>
                <label><input <?php echo $info['mailMethod']==1 ? "checked" : NULL; ?> type="radio" name="mailMethod" class="icheck" value="1"> SMTP</label>
                <label><input <?php echo $info['mailMethod']==2 ? "checked" : NULL; ?> type="radio" name="mailMethod" class="icheck" value="2"> PHP mail function</label>
            </div>
        </div>    
        <div class="form-group hidden">
            <label class="col-sm-2 control-label" >SMTP Authenticate</label>
            <div class="col-sm-7 icheck-inline">
                <?php  $info['mailAuthenticate'] = isset($info['mailAuthenticate']) ? $info['mailAuthenticate'] : 1 ?>
                <label><input  <?php echo $info['mailAuthenticate']==1 ? "checked" : NULL; ?> type="radio" name="mailAuthenticate" class="icheck" value="1" checked> Yes</label>
                <label><input  <?php echo $info['mailAuthenticate']==2 ? "checked" : NULL; ?> type="radio" name="mailAuthenticate" class="icheck" value="2"> No</label>
            </div>
        </div>                  
        <div class="form-group">
            <label class="col-sm-2 control-label" >SMTP Sever</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['SMTPserver']) ? $info['SMTPserver'] : NULL; ?>" type="text" id="" class="form-control" name="SMTPserver">
            </div>
        </div> 
        <div class="form-group">
            <label class="col-sm-2 control-label" >SMTP Port</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['SMTPport']) ? $info['SMTPport'] : NULL; ?>" type="text" id="" class="form-control" name="SMTPport">
            </div>
        </div>     
        <div class="form-group">
            <label class="col-sm-2 control-label" >SMTP username</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['SMTPusername']) ? $info['SMTPusername'] : NULL; ?>" type="text" id="" class="form-control" name="SMTPusername">
            </div>
        </div>       
        <div class="form-group">
            <label class="col-sm-2 control-label" >SMTP password</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['SMTPpassword']) ? $info['SMTPpassword'] : NULL; ?>" type="password" id="" class="form-control" name="SMTPpassword">
            </div>
        </div>                  
        <h4 class="block">Sender mail</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >Sender name</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['senderName']) ? $info['senderName'] : NULL; ?>" type="text" id="" class="form-control" name="senderName">
            </div>
        </div>     
        <div class="form-group">
            <label class="col-sm-2 control-label" >Sender Email</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['senderEmail']) ? $info['senderEmail'] : NULL; ?>" type="text" id="" class="form-control" name="senderEmail">
            </div>
        </div>                  
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <?php echo form_close() ?>
</div>
