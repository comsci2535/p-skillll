<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="box-body">
        <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-create', 'method' => 'post')) ?>
        <h4 class="block">ข้อมูลทั่วไป</h4>                           
            
        
        
        
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อ</label>
            <div class="col-sm-7">
                <input value="" type="text" id="input-username" class="form-control" name="firstname" required>
            </div>
        </div>  

        <div class="form-group">
            <label class="col-sm-2 control-label" >นามสกุล</label>
            <div class="col-sm-7">
                <input value="" type="text" id="input-username" class="form-control" name="lastname">
            </div>
        </div> 

        <div class="form-group">
            <label class="col-sm-2 control-label" >อีเมล์</label>
            <div class="col-sm-7">
                <input value="" type="email" id="input-email" class="form-control" name="email"  >
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" >เบอร์โทร</label>
            <div class="col-sm-7">
                <input value="" type="text" id="input-phone" class="form-control" name="phone">
            </div>
        </div>
       
        
       <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อผู้ใช้งาน</label>
            <div class="col-sm-7">
                <input value="" type="text" id="input-username" class="form-control" name="username">
            </div>
        </div>  
        
        <div class="form-group">
            <label class="col-sm-2 control-label">รหัสผ่าน</label>
            <div class="col-sm-7">
                <input value="" type="password" id="input-password" class="form-control" name="password" required>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">ยืนยันรหัสผ่าน</label>
            <div class="col-sm-7">
                <input value="" type="password" id="input-repassword" class="form-control" name="rePassword">
            </div>
        </div>
        
        <!-- <div class="form-group">
            <label class="col-sm-2 control-label">รูปภาพ</label>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-12" style="margin-bottom: 10px">
                        <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                        <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-success" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-folder"></i> คลังรูปภาพ</button>
                    </div>
                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                </div>
            </div>
        </div> -->
   
    </div>
    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-2">
            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->userId) ? encode_id($info->userId) : 0 ?>">
    <?php echo form_close() ?>
</div>           

<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style="">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-4" id="action">
        <div class="btn-group">
            <button onclick="set_cropper($(this))" type="button" title="แก้ไข" class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-crop"><i class="fa fa-crop"></i> ตัดภาพ</button>
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>
