<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ck_file_model extends CI_Model {
    public $filePath;
    public function __construct() {
        parent::__construct();
        $this->filePath = "uploads/ck/";
    }
    public function resize($filename, $width, $height) {
        
        if (!file_exists($this->filePath . $filename) || !is_file($this->filePath . $filename)) {
            return FALSE;
        }
       // arrx('a');
        $info = pathinfo($filename);
        $extension = $info['extension'];
        $image = array(
            'jpg',
            'jpeg',
            'png',
            'gif',
        );
        if (in_array(strtolower($extension), $image)) {
            $old_image = $filename;
            $new_image = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

            if (!file_exists($this->filePath. $new_image) || (filemtime($this->filePath  . $old_image) > filemtime($this->filePath  . $new_image))) {
                $path = '';
                $directories = explode('/', dirname($new_image));
                foreach ($directories as $directory) {
                    $path = $path . '/' . $directory;

                    if (!file_exists($this->filePath . $path)) {
                        @mkdir($this->filePath . $path, 0777);
                    }
                }
                $image = new Images_oc();
                $image->fileinfo($this->filePath . $old_image);
                $image->resize($width, $height);
                $image->save($this->filePath . $new_image);
            }

            return base_url('uploads/ck/') . $new_image;
        } else {
            return base_url('uploads/ck/no_image.png');
        }
    }

}

?>