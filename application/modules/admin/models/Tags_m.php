<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tags_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('tags a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('tags a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.tagsName', $param['keyword'])
                    ->or_like('a.tagsExcerpt', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.tagsDateCreate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.tagsDateUpdate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['tagsActive']) && $param['tagsActive'] != "" ) {
            $this->db->where('a.tagsActive', $param['tagsActive']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.tagsName', $param['search']['value'])
                    ->or_like('a.tagsExcerpt', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.tagsName";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.tagsExcerpt";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.tagsDateCreate";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.tagsDateUpdate";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.tagsRecycleDate";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['tagsID']) ) 
            $this->db->where('a.tagsID', $param['tagsID']);

         if ( isset($param['tagsName']) ) 
            $this->db->where('a.tagsName', $param['tagsName']);

        
        if ( isset($param['tagsRecycle']) ){
            if (is_array($param['tagsRecycle'])) {
                $this->db->where_in('a.tagsRecycle', $param['tagsRecycle']);
            } else {
                $this->db->where('a.tagsRecycle', $param['tagsRecycle']);
            }
           
        }

    }
    
    public function insert($value) {
        $this->db->insert('tags', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('tagsID', $id)
                        ->update('tags', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('tagsID', $id)
                        ->update('tags', $value);
        return $query;
    }    

}
