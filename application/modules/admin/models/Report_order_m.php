<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_order_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                         ->select('b.title')
                         ->select('c.firstname,c.lastname')
                        ->from('course_register a')
                        ->join('course b', 'a.courseId = b.courseId', 'left')
                        ->join('user c', 'a.userId = c.userId', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title')
                        ->select('c.firstname,c.lastname')
                        ->from('course_register a')
                        ->join('course b', 'a.courseId = b.courseId', 'left')
                        ->join('user c', 'a.userId = c.userId', 'left')
                        ->get();
        return $query->num_rows();
    }

    public function get_instructor_map_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
         
        $query = $this->db
                        ->select('a.*')
                        ->select('b.title')
                        ->select('c.firstname,c.lastname')
                        ->select('d.userId')
                        ->from('course_register a')
                        ->join('course b', 'a.courseId = b.courseId', 'left')
                        ->join('user c', 'a.userId = c.userId', 'left')
                        ->join('course_instructor_map d', 'b.courseId = d.courseId')
                        ->get(); 
        return $query;
    } 

    public function get_instructor_map_count($param) 
    {
        $this->_condition($param);

        $query = $this->db
                        ->select('a.*')
                        ->select('b.title')
                        ->select('c.firstname,c.lastname')
                        ->select('d.userId')
                        ->from('course_register a')
                        ->join('course b', 'a.courseId = b.courseId', 'left')
                        ->join('user c', 'a.userId = c.userId', 'left')
                        ->join('course_instructor_map d', 'b.courseId = d.courseId')
                        ->get(); 
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['keyword'])
                    ->or_like('a.code', $param['keyword'])
                    ->or_like('c.firstname', $param['keyword'])
                    ->or_like('c.lastname', $param['keyword'])
                     ->or_like('a.couponCode', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updateDate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['status']) && $param['status'] != "" ) {
            $this->db->where('a.status', $param['status']);
        } 

        if ( isset($param['courseId']) && $param['courseId'] != "" ) {
            $this->db->where('a.courseId', $param['courseId']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.code";
            if ($param['order'][0]['column'] == 2) $columnOrder = "b.title";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 6) $columnOrder = "a.createDate";
               
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycleDate";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['course_registerId']) ) 
            $this->db->where('a.course_registerId', $param['course_registerId']); 
            
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

        if(!empty($param['userId'])){ 
            $this->db->where('d.userId', $param['userId']);
        }

    }
    
    public function insert($value) {
        $this->db->insert('course_register', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('course_registerId', $id)
                        ->update('course_register', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('course_registerId', $id)
                        ->update('course_register', $value);
        return $query;
    } 

    public function insert_course_member($value)
    {

      return $this->db->insert('course_member', $value); 

    }  

     public function del_course_member($id)
    {

        $query = $this->db

                ->where('code', $id)

                ->delete('course_member');

        return $query;

    }  

     public function plus_learner($id)
    {
        $sql = "UPDATE course SET learner = (learner+1) WHERE courseId=?";
        $this->db->query($sql, array($id));
    }  

}
