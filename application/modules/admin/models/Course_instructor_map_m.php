<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Course_instructor_map_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_course_instructor_map($param) { 

        if(!empty($param['courseId'])){
            $this->db->where('a.courseId', $param['courseId']);
        }

        if(!empty($param['userId'])){
            $this->db->where('a.userId', $param['userId']);
        } 
        
        $query = $this->db->get('course_instructor_map a');
        return $query;
    } 
}
